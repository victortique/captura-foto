//
//  CapturaFotoApp.swift
//  CapturaFoto
//
//  Created by Victor Tique on 19/06/22.
//

import SwiftUI

@main
struct CapturaFotoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
